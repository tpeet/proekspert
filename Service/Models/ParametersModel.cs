﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;

namespace Service.Models
{
    public class ParametersModel
    {
        /// <summary>
        /// Parameter used for querying legacy data
        /// </summary>
        public int RequestParameter { get; set; }

        /// <summary>
        /// Shows if service is active
        /// </summary>
        public bool? IsServiceActive { get; set; }

        /// <summary>
        /// Phonenumber from service parameters
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Shows if XL-service is active
        /// </summary>
        public bool? IsXLServiceActive { get; set; }

        /// <summary>
        /// Shows the language of the service
        /// </summary>
        public Language? ServiceLanguage { get; set; }

        /// <summary>
        /// Shows the language of the XL-service
        /// </summary>
        public Language? XlServiceLanguage { get; set; }

        /// <summary>
        /// Shows when is the service ending
        /// </summary>
        public DateTime? ServiceEndTime { get; set; }

        /// <summary>
        /// Shows when was the XL-service activated
        /// </summary>
        public DateTime? XLServiceActivitonTime { get; set; }

        /// <summary>
        /// Shows when was the XL-service ended
        /// </summary>
        public DateTime? XLServiceEndTime { get; set; }

        /// <summary>
        /// Shows if exception list is in use
        /// </summary>
        public bool? IsOverrideInUse { get; set; }

        /// <summary>
        /// Shows the list of numbers in the exception list
        /// </summary>
        public List<string> OverridePhoneNumbers { get; set; } 

        /// <summary>
        /// Shows the list of names in the exception list
        /// </summary>
        public List<string> OverrideNames { get; set; }

        /// <summary>
        /// Shows whether the legacy service files was found or not
        /// </summary>
        public bool IsFileNotFound { get; set; }

        /// <summary>
        /// Shows whether the data from legacy service was formatted correctly or not
        /// </summary>
        public bool IsWrongFormat { get; set; }
    }

    /// <summary>
    /// Collection of available languages
    /// </summary>
    public enum Language { Estonian, English };
}