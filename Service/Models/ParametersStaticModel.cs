﻿using System;
using System.Collections.Generic;

namespace Service.Models
{
    public static class ParametersStaticModel
    {
        public static int RequestParameter { get; set; }
        public static bool? IsServiceActive { get; set; }
        public static string PhoneNumber { get; set; }
        public static bool? IsXLServiceActive { get; set; }
        public static Language? ServiceLanguage { get; set; }
        public static Language? XlServiceLanguage { get; set; }
        public static DateTime? ServiceEndTime { get; set; }
        public static DateTime? XLServiceActivitonTime { get; set; }
        public static DateTime? XLServiceEndTime { get; set; }
        public static bool? IsOverrideInUse { get; set; }
        public static List<string> OverridePhoneNumbers { get; set; }
        public static List<string> OverrideNames { get; set; }
        public static bool IsFileNotFound { get; set; }
        public static bool IsWrongFormat { get; set; }
    }

}