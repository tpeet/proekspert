﻿using System.Web.Http;
using Service.Models;

namespace Service.Controllers
{
    [RoutePrefix("api/Service")]
    public class ServiceController : ApiController
    {
        // GET api/Service/Parameters
        /// <summary>
        /// Gets data from legacy interface. Data is updated every 5 seconds, so that after each update a new
        /// requestparameter is used. Requestparameters are in range from 1 to 9 and the value changes after every 5 seconds.
        /// After returning the value from the 9th request parameter, it starts again from the 1st.
        /// </summary>
        /// <returns>Returns data in JSON format</returns>
        [Route("Parameters")]
        public ParametersModel GetParameters()
        {
            return BL.ServiceLogic.GetParametersModel();
        }
    }
}
