﻿using System;
using System.Collections.Generic;
using Service.Models;

namespace Service.DAL
{
    public static class LegacyParser
    {
        public static ParametersModel GetParametersModel(string legacyData)
        {
            return new ParametersModel
            {
                IsServiceActive = GetServiceActivity(legacyData),
                PhoneNumber = GetPhoneNumber(legacyData),
                IsXLServiceActive = GetXLServiceActivity(legacyData),
                ServiceLanguage = GetServiceLanguage(legacyData),
                XlServiceLanguage = GetXLServiceLanguage(legacyData),
                ServiceEndTime = GetServiceEndTime(legacyData),
                XLServiceActivitonTime = GetXLServiceActivitionTime(legacyData),
                XLServiceEndTime = GetXLServiceEndTime(legacyData),
                IsOverrideInUse = GetOverrideUsage(legacyData),
                OverrideNames = GetOverrideNames(legacyData),
                OverridePhoneNumbers = GetOverridePhoneNumbers(legacyData)
            };
        }

        public static bool GetServiceActivity(string legacyData)
        {
            if (legacyData.Length < 1)
                throw new FormatException("The legacyData string must have at least 1 char");

            var serviceActiveChar = legacyData[0];
            if (serviceActiveChar.Equals('A'))
                return true;
            else if (serviceActiveChar.Equals('P'))
                return false;
            else
                throw new FormatException("The argument for defining whether service is active or not, in legacy data, wasn't formatted correctly");
        }

        public static string GetPhoneNumber(string legacyData)
        {
            return legacyData.Length < 21 ? null : legacyData.Substring(1, 20);
        }

        public static bool? GetXLServiceActivity(string legacyData)
        {
            if (legacyData.Length < 22)
                return null;

            var XLserviceActiveChar = legacyData[21];
            if (XLserviceActiveChar.Equals('J'))
                return true;
            else if (XLserviceActiveChar.Equals('E'))
                return false;
            else
                throw new FormatException("The argument for defining whether XL service is active or not, in legacy data, wasn't formatted correctly");
        }

        public static Language? GetServiceLanguage(string legacyData)
        {
            if (legacyData.Length < 23)
                return null;
            var serviceLanguage = legacyData[22];
            if (serviceLanguage.Equals('E'))
                return Language.Estonian;
            else if (serviceLanguage.Equals('I'))
                return Language.English;
            else
                throw new FormatException("The argument for defining service language in legacy data, wasn't formatted correctly");
        }

        public static Language? GetXLServiceLanguage(string legacyData)
        {
            if (legacyData.Length < 24)
                return null;
            var XLserviceLanguage = legacyData[23];
            if (XLserviceLanguage.Equals('E'))
                return Language.Estonian;
            else if (XLserviceLanguage.Equals('I'))
                return Language.English;
            else if (XLserviceLanguage.Equals(' '))
                return null;
            else
                throw new FormatException("The argument for defining XL service language in legacy data, wasn't formatted correctly");
        }

        public static DateTime? GetServiceEndTime(string legacyData)
        {
            if (legacyData.Length < 36)
                return null;
            var endTimeString = legacyData.Substring(24, 12);
            var endTime = DateTime.ParseExact(endTimeString, "yyyyMMddHHmm",
                System.Globalization.CultureInfo.InvariantCulture);
            return endTime;
        }

        public static DateTime? GetXLServiceActivitionTime(string legacyData)
        {
            if (legacyData.Length < 40)
                return null;
            var timeString = legacyData.Substring(36, 4);
            if (String.IsNullOrWhiteSpace(timeString)) return null;
            return DateTime.ParseExact(timeString, "HHmm",
                System.Globalization.CultureInfo.InvariantCulture);
        }

        public static DateTime? GetXLServiceEndTime(string legacyData)
        {
            if (legacyData.Length < 44)
                return null;
            var timeString = legacyData.Substring(40, 4);
            if (String.IsNullOrWhiteSpace(timeString)) return null;
            return DateTime.ParseExact(timeString, "HHmm",
                System.Globalization.CultureInfo.InvariantCulture);
        }

        public static bool? GetOverrideUsage(string legacyData)
        {
            if (legacyData.Length < 45)
                return null;
            var overrideInUseChar = legacyData[44];
            if (overrideInUseChar.Equals('K'))
                return true;
            else if (overrideInUseChar.Equals('E'))
                return false;
            else
                throw new FormatException("The argument for defining whether override list is in use, in legacy data, wasn't formatted correctly");
        }

        public static List<string> GetOverridePhoneNumbers(string legacyData)
        {
            if (legacyData.Length < 165)
                return null;

            var overridePhoneNumbersString = legacyData.Substring(45, 120);
            return SplitToEqualParts(overridePhoneNumbersString, 15);
        }

        public static List<String> GetOverrideNames(string legacyData)
        {
            if (legacyData.Length < 325)
                return null;

            var overrideNamesString = legacyData.Substring(165, 160);
            return SplitToEqualParts(overrideNamesString, 20);
        }

        // http://stackoverflow.com/questions/1450774/splitting-a-string-into-chunks-of-a-certain-size
        private static List<String> SplitToEqualParts(string str, int chunkSize)
        {
            var list = new List<string>();
            for (var i = 0; i < str.Length; i += chunkSize)
            {
                var substring = str.Substring(i, chunkSize);
                if (!String.IsNullOrWhiteSpace(substring))
                    list.Add(substring);
            }
            return list;
        }
    }
}