﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using Service.Models;

namespace Service.DAL
{
    public class LegacyService
    {
        public static String GetLegacyData(int requestParameter)
        {
            var fileLocation = String.Format(ConfigurationManager.AppSettings["legacyDataUrl"], requestParameter);

            var clients = new WebClient();
            Stream stream = clients.OpenRead(fileLocation);


            if (stream == null) return null;
            var reader = new StreamReader(stream);
            return reader.ReadToEnd();
        }

        public static ParametersModel GetParametersModel(int requestParameter)
        {
            String legacyData;
            try
            {
                legacyData = GetLegacyData(requestParameter);
            }
            catch (WebException)
            {
                return new ParametersModel
                {
                    RequestParameter = requestParameter,
                    IsFileNotFound = true,
                };
            }
            if (legacyData == null || legacyData.Length <= 0) return null;
            ParametersModel parametersModel = new ParametersModel();
            try
            {
                parametersModel = LegacyParser.GetParametersModel(legacyData);
            }

            catch (FormatException)
            {
                parametersModel.IsWrongFormat = true;
            }
            
            parametersModel.RequestParameter = requestParameter;
            return parametersModel;
        }

    }
}