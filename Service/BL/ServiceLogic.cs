﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Service.DAL;
using Service.Models;

namespace Service.BL
{
    public static class ServiceLogic
    {
        public static ParametersModel GetParametersModel()
        {
            return LoadModel();
        }

        public static void StartPolling()
        {
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = Int32.Parse(ConfigurationManager.AppSettings["updateInterval"]); ;
            timer.Elapsed += GetAndSaveModel;
            timer.Start();
        }

        private static void GetAndSaveModel(object sender, System.Timers.ElapsedEventArgs e)
        {
            var minParameter = Int32.Parse(ConfigurationManager.AppSettings["minReqParameter"]);
            var maxParameter = Int32.Parse(ConfigurationManager.AppSettings["maxReqParameter"]);
            var requestNumber = ParametersStaticModel.RequestParameter + 1;
            if (ParametersStaticModel.RequestParameter < minParameter || ParametersStaticModel.RequestParameter >= maxParameter)
                requestNumber = minParameter;
            var parametersModel = LegacyService.GetParametersModel(requestNumber) ??
                                  new ParametersModel {RequestParameter = requestNumber};
            SaveModel(parametersModel);
        }

        private static void SaveModel(ParametersModel parametersModel)
        {
            ParametersStaticModel.RequestParameter = parametersModel.RequestParameter;
            ParametersStaticModel.PhoneNumber = parametersModel.PhoneNumber;
            ParametersStaticModel.OverridePhoneNumbers = parametersModel.OverridePhoneNumbers;
            ParametersStaticModel.OverrideNames = parametersModel.OverrideNames;
            ParametersStaticModel.IsXLServiceActive = parametersModel.IsXLServiceActive;
            ParametersStaticModel.IsServiceActive = parametersModel.IsServiceActive;
            ParametersStaticModel.IsOverrideInUse = parametersModel.IsOverrideInUse;
            ParametersStaticModel.ServiceEndTime = parametersModel.ServiceEndTime;
            ParametersStaticModel.ServiceLanguage = parametersModel.ServiceLanguage;
            ParametersStaticModel.XLServiceActivitonTime = parametersModel.XLServiceActivitonTime;
            ParametersStaticModel.XLServiceEndTime = parametersModel.XLServiceEndTime;
            ParametersStaticModel.XlServiceLanguage = parametersModel.XlServiceLanguage;
            ParametersStaticModel.IsFileNotFound = parametersModel.IsFileNotFound;
            ParametersStaticModel.IsWrongFormat = parametersModel.IsWrongFormat;
        }

        private static ParametersModel LoadModel()
        {
            return new ParametersModel
            {
                IsOverrideInUse = ParametersStaticModel.IsOverrideInUse,
                IsServiceActive = ParametersStaticModel.IsServiceActive,
                IsXLServiceActive = ParametersStaticModel.IsXLServiceActive,
                OverrideNames = ParametersStaticModel.OverrideNames,
                OverridePhoneNumbers = ParametersStaticModel.OverridePhoneNumbers,
                PhoneNumber = ParametersStaticModel.PhoneNumber,
                RequestParameter = ParametersStaticModel.RequestParameter,
                ServiceEndTime = ParametersStaticModel.ServiceEndTime,
                ServiceLanguage = ParametersStaticModel.ServiceLanguage,
                XLServiceActivitonTime = ParametersStaticModel.XLServiceActivitonTime,
                XLServiceEndTime = ParametersStaticModel.XLServiceEndTime,
                XlServiceLanguage = ParametersStaticModel.XlServiceLanguage,
                IsFileNotFound = ParametersStaticModel.IsFileNotFound,
                IsWrongFormat = ParametersStaticModel.IsWrongFormat
            };
        }
    }
}