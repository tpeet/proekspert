This project consists of web service, a client and unit tests. The Web service queries data from legacy service every 5 seconds. Each query gets information about one of the service parameters sets available. Each query has a request parameter which varies from 1 to 9. With each update new service parameters are read into memory, corresponding to the request parameter, and can be accessed by API which returns JSON object. After querying the last  paramameters set, it starts again from request parameter 1. API documentation can be seen, when web service is running (default address: http://localhost:49932/service/Help)

Web client access web service through API GET requests and get's the info about service parameters. It updates the page automatically, so it's possible to constantly monitor all the service parameters sets.

Unit tests are currently testing the parsing of legacy data.

# Requirements
Minimum Visual Studio version for opening the solution is Visual Studio 2010 SP1. 

The target .NET version is 4.6 and the solution might not compile with older versions.


# Building and running guide 
Download *.zip file with the project from bitbucket or use git to clone it to your preferred directory:
~~~~
git clone https://tpeet@bitbucket.org/tpeet/proekspert.git
~~~~

Open solution in Visual Studio (tested with Visual Studio 2015 Community and Visual Studio 2013 Professional) and make sure Client project is selected as startup project (right click on the project and select 'Set up as StartUp Project'). Also make sure that "Only build startup projects and dependencies on Run" is NOT selected in Visual Studio options (Tools -> Options -> Projects and Solutions -> Build and run). After this you can run the project. On the first run NuGeT package manager adds all the necessary pagackes after which a web page should open in your web browser which shows formatted data from legacy service.

Building the code without Visual Studio:
* http://stackoverflow.com/questions/18286855/how-can-i-compile-and-run-c-sharp-program-without-using-visual-studio

# Setup guide
Most of the important settings can be found between the <appSettings></appSettings> elements in web.config files. In Service project, the web.config has the following options:

+ *minReqParameter* - minimal request parameter used for queryung legacy data *(default 1)*
+ *maxReqParameter* - maximal request parameter used for querying legacy data *(default 9)*
+ *updateInterval* - the time interval in milliseconds in which the web service updates the data from legacy service *(default: 5000)*
+ *legacyDataUrl* - the address of the legacy data, where *{0}* is being replaced by a number between *minReqParameter* and *maxReqParameter*  *(default: http://people.proekspert.ee/ak/data_{0}.txt)*

Client service has the following option:

* *ServiceURL* - this shows the address of the web service where to get formatted JSON object with service parameters set *(default http://localhost:49932/service/api/Service/Parameters)*

The interval of Client update rate can be changed in *Scripts/custom.js* under the *interval* variable

