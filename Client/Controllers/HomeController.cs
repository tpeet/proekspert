﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Client.BL;
using Client.Models;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var parametersModel = Service.GetParametersModel();
            return View(parametersModel);
        }

        public ActionResult Refresh()
        {
            var parametersModel = Service.GetParametersModel();
            return  Json(RenderRazorViewToString("~/Views/Home/index.cshtml", parametersModel), JsonRequestBehavior.AllowGet); 
        }


        // http://stackoverflow.com/questions/483091/render-a-view-as-a-string
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}