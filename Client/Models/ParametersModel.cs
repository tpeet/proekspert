﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Client.Models
{
    public class ParametersModel
    {
        public int RequestParameter { get; set; }
        public bool? IsServiceActive { get; set; }
        public string PhoneNumber { get; set; }
        public bool? IsXLServiceActive { get; set; }
        public Language? ServiceLanguage { get; set; }
        public Language? XlServiceLanguage { get; set; }
        [DisplayFormat(DataFormatString = "{0:MMMM d yyyy HH.mm}")]
        public DateTime? ServiceEndTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:HH.mm}")]
        public DateTime? XLServiceActivitonTime { get; set; }
        [DisplayFormat(DataFormatString = "{0:HH.mm}")]
        public DateTime? XLServiceEndTime { get; set; }
        public bool? IsOverrideInUse { get; set; }
        public List<string> OverridePhoneNumbers { get; set; } 
        public List<string> OverrideNames { get; set; } 
        public bool IsFileNotFound { get; set; }
        public bool IsWrongFormat { get; set; }
    }

    public enum Language { Estonian, English };
}