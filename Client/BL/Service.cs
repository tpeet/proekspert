﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using Client.Models;

namespace Client.BL
{
    public static class Service
    {
        public static ParametersModel GetParametersModel()
        {
            string json;
            using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString(ConfigurationManager.AppSettings["ServiceURL"]);
            }
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ParametersModel>(json);
        }
    }
}