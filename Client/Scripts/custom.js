﻿var interval = 2500;
setInterval(updateContent, interval);

function updateContent() {
    $.get("/Home/Refresh", null, 
        function (response) { 
            $("div.content").html(response);
        },
        "json"
    );
}