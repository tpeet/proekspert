﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.DAL;
using Service.Models;

namespace Proekspert.Tests.DAL
{
    [TestClass]
    public class LegacyParserTest
    {


        [TestMethod]
        public void GetServiceActivity()
        {
            Assert.IsTrue(LegacyParser.GetServiceActivity("A123012310123"));
            Assert.IsFalse(LegacyParser.GetServiceActivity("P123012310123"));
            try
            {
                LegacyParser.GetServiceActivity("");
                Assert.Fail("Empty string must throw FormatException error");
            }
            catch (FormatException) { }

            try
            {
                LegacyParser.GetServiceActivity("B3523525253235");
                Assert.Fail("In case of a wrong first letter a FormatException must be thrown");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void GetPhoneNumbers()
        {
            Assert.AreEqual(LegacyParser.GetPhoneNumber("A0551234567          JE 20111023160008001200E"), "0551234567          ");
            Assert.AreEqual(LegacyParser.GetPhoneNumber("A0551234569          JII20111023235908001200K0551111111     0509999999                                                                                               Jaan Juurikas       Peeter                                                                                                                                      "), "0551234569          ");
            Assert.IsNull(LegacyParser.GetPhoneNumber("A012"));
        }

        [TestMethod]
        public void GetXLServiceActivity()
        {
            Assert.IsTrue(LegacyParser.GetXLServiceActivity("A0551234567          JE 20111023160008001200E") == true);
            Assert.IsTrue(LegacyParser.GetXLServiceActivity("P0502234569          EE 201201012359        E") == false);
            Assert.IsNull(LegacyParser.GetXLServiceActivity("B3523525"));
            try
            {
                LegacyParser.GetXLServiceActivity("P0502234569          SE 201201012359        E");
                Assert.Fail("FormatException must be thrown when XL service activity isn't marked as 'J' or 'E'");
            }
            catch (FormatException) { }

        }

        [TestMethod]
        public void GetServiceLanguage()
        {
            Assert.IsTrue(LegacyParser.GetServiceLanguage("A0551234567          JE 20111023160008001200E") == Language.Estonian);
            Assert.IsTrue(LegacyParser.GetServiceLanguage("P0502234569          EI 201201012359        E") == Language.English);
            Assert.IsNull(LegacyParser.GetServiceLanguage("B3523525"));
            try
            {
                LegacyParser.GetServiceLanguage("P0502234569          JK 201201012359        E");
                Assert.Fail("FormatException must be thrown when service language isn't marked as 'E' or 'I'");
            }
            catch (FormatException) { }

        }

        [TestMethod]
        public void GetXLServiceLanguage()
        {
            Assert.IsTrue(LegacyParser.GetXLServiceLanguage("A0551234567          JEE20111023160008001200E") == Language.Estonian);
            Assert.IsTrue(LegacyParser.GetXLServiceLanguage("P0502234569          JII201201012359        E") == Language.English);
            Assert.IsNull(LegacyParser.GetXLServiceLanguage("P0502234569          EI 201201012359        E"));
            Assert.IsNull(LegacyParser.GetXLServiceLanguage("B3523525"));
            try
            {
                LegacyParser.GetXLServiceLanguage("P0502234569          JEK201201012359        E");
                Assert.Fail("FormatException must be thrown when XL service language isn't marked as 'E' or 'I'");
            }
            catch (FormatException) { }

        }

        [TestMethod]
        public void GetServiceEndTime()
        {
            Assert.IsTrue(LegacyParser.GetServiceEndTime("A0551234567          JEE20111023160008001200E") == Convert.ToDateTime("23/10/2011 16:00"));
            Assert.IsFalse(LegacyParser.GetServiceEndTime("A0551234567          JEE20111023160508001200E") == Convert.ToDateTime("23/10/2011 16:00"));
            Assert.IsNull(LegacyParser.GetServiceEndTime("B3523525"));

            try
            {
                // Service end time is 2500
                LegacyParser.GetServiceEndTime("A0551234567          JEE20111023250008001200E");
                Assert.Fail("Must throw FormatException if time is not formatted correctly");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void GetXLServiceActivitionTime()
        {
            Assert.IsTrue(LegacyParser.GetXLServiceActivitionTime("A0551234567          JEE20111023160008001200E") == Convert.ToDateTime("08:00"));
            Assert.IsFalse(LegacyParser.GetXLServiceActivitionTime("A0551234567          JEE20111023160508001200E") == Convert.ToDateTime("16:00"));
            Assert.IsNull(LegacyParser.GetXLServiceActivitionTime("B3523525"));

            try
            {
                // XL service activition time is 3400
                LegacyParser.GetXLServiceActivitionTime("A0551234567          JEE20111023250034001200E");
                Assert.Fail("Must throw FormatException if time is not formatted correctly");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void GetXLServiceEndTime()
        {
            Assert.IsTrue(LegacyParser.GetXLServiceEndTime("A0551234567          JEE20111023160008001200E") == Convert.ToDateTime("12:00"));
            Assert.IsFalse(LegacyParser.GetXLServiceEndTime("A0551234567          JEE20111023160508001200E") == Convert.ToDateTime("16:00"));
            Assert.IsNull(LegacyParser.GetXLServiceEndTime("B3523525"));

            try
            {
                // XL service end time is 2700
                LegacyParser.GetXLServiceEndTime("A0551234567          JEE20111023250008002700E");
                Assert.Fail("Must throw FormatException if time is not formatted correctly");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void GetOverrideUsage()
        {
            Assert.IsTrue(LegacyParser.GetOverrideUsage("A0551234555          JII20111111215900001200K0552212211     0506669999                                                                                               Rein Ratas                                                                                                                                                      ") == true);
            Assert.IsTrue(LegacyParser.GetOverrideUsage("A0551234567          JE 20111023160008001200E") == false);

            try
            {
                LegacyParser.GetOverrideUsage("A0551234567          JE 20111023160008001200F");
                Assert.Fail("FormatException must be thrown when override lists in use char isn't 'K' or 'E'");
            }
            catch (FormatException) { }
        }

        [TestMethod]
        public void GetOverridePhoneNumbers()
        {
            var phoneNumbers =
                LegacyParser.GetOverridePhoneNumbers(
                    "A0551234555          JII20111111215900001200K0552212211     0506669999                                                                                               Rein Ratas                                                                                                                                                      ");
            Assert.AreEqual(phoneNumbers.Count, 2);
            Assert.AreEqual(phoneNumbers[0], "0552212211     ");
            Assert.AreEqual(phoneNumbers[1], "0506669999     ");
            Assert.IsNull(LegacyParser.GetOverridePhoneNumbers("A0551234567          JE 20111023160008001200F"));
        }

        [TestMethod]
        public void GetOverrideNames()
        {
            var names =
                LegacyParser.GetOverrideNames(
                    "A0551234555          JII20111111215900001200K0552212211     0506669999                                                                                               Rein Ratas                                                                                                                                                      ");
            Assert.AreEqual(names.Count, 1);
            Assert.AreEqual(names[0], "Rein Ratas          ");
            Assert.IsNull(LegacyParser.GetOverrideNames("A0551234567          JE 20111023160008001200F"));
        }
    }
}
